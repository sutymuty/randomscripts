#ifndef WINDOW_HPP_INCLUDED
#define WINDOW_HPP_INCLUDED
#include "graphics.hpp"
#include "iroswidget.hpp"
#include "gomb.hpp"
#include <vector>

using namespace std;

class window{
private:
    genv::event ev;
    vector<iros*> i;
    vector<gomb*> g;
    int mx,my;
public:
    void loop();
};


#endif // WINDOW_HPP_INCLUDED
