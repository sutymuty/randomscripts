#ifndef MENU_HPP_INCLUDED
#define MENU_HPP_INCLUDED

#include "graphics.hpp"
#include "oswidget.hpp"
#include "gomb.hpp"
#include "szam.hpp"
#include "legordulo.hpp"
#include "iroswidget.hpp"
#include "MapElement.hpp"
#include <vector>

///menu cpp

using namespace std;
using namespace genv;

const int XX=1366;  ///ha itt atirod, akkor a window.cpp is pls
const int YY=768;

class menu{
private:
    int mx,my;  ///focus
    iros *i2 = new iros(XX/2+50,YY-650,150,50,"Player 2");              ///nev2 masodik jatekos
    iros *i1 = new iros(XX/2-250,YY-650,150,50,"Player 1");                ///nev1 elso jatekos
    legor *l = new legor(XX/2-75,YY-550,100,50,5);          ///pálya mérete
    szamos *s2 = new szamos(XX/2-25,YY-400,50,50,3,10);     ///hányas amőba
    szamos *s1 = new szamos(XX/2-25,YY-300,50,50,10,60);     ///idő (másodperc)
public:
    gomb *g = new gomb(XX-200,YY-200,100,50,"START");     ///start
    bool handler(event ev,int &time,int &mennyi, int &hany,vector<vector<MElement*>> &p,string &a, string &b,int &malac);   ///kezelo
    virtual void draw();    ///rajzolo
};


#endif // MENU_HPP_INCLUDED
