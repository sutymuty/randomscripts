#include "graphics.hpp"
#include "oswidget.hpp"
#include "iroswidget.hpp"
#include <string>

///iros widget cppp

using namespace std;
using namespace genv;

iros::iros(double a, double b, double c, double d,string s) : oswidget(a,b,c,d){
    szoveg=s;       /// szoveg ami epp a boxban van
    can = true;     ///lehet-e irni
}

void iros::ad(char a){  ///szoveg beirasa
    if (gout.twidth(szoveg)> _sx-25){
        can = false;    /// ha nem ferne ki
    }else{
        can = true;
    }
    if (can) szoveg+=a; ///ha van hely lehet irni

}

void iros::del(){   ///szovegtorlese
    szoveg = szoveg.substr(0, szoveg.size()-1);

}

void iros::draw() const{    ///kirajzolas
    gout << move_to(_x,_y)<<color(0,64,0)<<box(_sx,_sy)<<move_to(_x+2,_y+2)<<color(255,255,255)<<box(_sx-4,_sy-4)<<move_to(_x+ 10,_y+_sy/2 +5)<<color(0,0,0)<<text(szoveg)<<text('|')<<refresh;
}

void iros::handler(event ev){ ///kezelo
    if (ev.pos_x!=0 || ev.pos_y!=0){
        mx=ev.pos_x;
        my=ev.pos_y;
    }
    if (selected(mx,my)){
        if (ev.type == ev_key){
                if (ev.keycode >= 21 && ev.keycode!=key_space) ad(ev.keycode);  ///gepeles
                if (ev.keycode == key_backspace) del(); ///torles
            }
    }
    draw();
}

string iros::getString() const{ ///szoveg getter
    return szoveg;
}
