#ifndef GOMB_HPP_INCLUDED
#define GOMB_HPP_INCLUDED

#include "graphics.hpp"
#include "oswidget.hpp"

///gomb widget hpp

class gomb : public oswidget{
private:
    std::string felirat;    ///felirat
    bool pressed;           ///akkor true, ha le volt nyomva
public:
    gomb(double a,double b, double c, double d, std::string szov);  ///kezdoX kezdoY hosszX hosszY szoveg
    virtual void draw() const;  ///kirajzolo
    virtual void handler(genv::event ev);   ///kezelo
    bool getStatus() const; ///Statusz getter
    void change();
    void setPressed();  ///visszaallito
    void setNotPressed();
};


#endif // GOMB_HPP_INCLUDED
