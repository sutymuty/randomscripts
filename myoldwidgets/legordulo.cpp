#include "graphics.hpp"
#include "oswidget.hpp"
#include "legordulo.hpp"
#include <vector>

using namespace std;
using namespace genv;

///legordulo cpp amobahoz TODO: lehet.pushback aradat delete es a reporter modositasa

int legor::reporter() const{  /// reported fuggveny, mekkora lesz a map
    int res=melyiket+2;
    if (melyiket==0) res=3;
return res;
}

void legor::clrscr() const{ /// rajzolashoz
    gout<<move_to(_x,_y)<<color(255,255,255) <<box(_sx,_sy+hany);
}

legor::legor(double a, double b,double c, double d,double han, vector<string> alma) : oswidget(a,b,c,d){  /// konstruktor (kezdő X,kezdő Y,hossz,magasság,hány elemet akarunk)
    lehet=alma;
    melyiket=0;                  /// melyik elemet irja majd ki a tombbol mint kivalasztott
    butselIs=0;                  /// paros szam eseten nincs legordules, paratlan szam eseten van
    mettol=1;                    /// legordulés eseten hanyadik elemtol szamolva mutasson 6 elemet a lista
    meddig=han;                   /// meddig irtuk ki;
    hany=han*16;                  /// mekkora lesz a legordulomenu merete
    success=false;               /// sikeresen talalt-e melyiket kell megjeleníteni
    if (meddig < 3) meddig=3;   /// minimum 3 valasztasi lehetoseg
    if (hany<16*3) hany=16*3;   /// meret korrigalas
    if (_sx<150) _sx = 150;     /// meret korrigalas
    if (_sy<40) _sy = 40;
}

void legor::addnew(string a){  /// uj elem hozzaadasa, nem használtam meg
    lehet.push_back(a);
}

void legor::textdraw() const{  /// valasztott szoveg kiirasa
    gout<<move_to(_x+12,_y+_sy/2+5)<<color(0,0,0)<<text(lehet[melyiket]);
}

void legor::draw() const{ ///grafikai panel
    gout<<move_to(_x,_y)<<color(0,64,0)<<box(_sx,_sy)<<move_to(_x+2,_y+2)<<color(255,255,255)<<box(_sx-4,_sy-4); /// kék keret
    gout<<move_to(_x+8,_y+8)<<color(0,0,0)<<line_to(_x+_sx-8,_y+8)<<line_to(_x+_sx-8,_y+_sy-8)<<line_to(_x+8, _y+_sy-8)<<line_to(_x+8,_y+8);///fekete keret |  későbbre: gombokgomb koordináta _x+_sx-30, _y+_sy-8
    gout<<move_to(_x+_sx-30,_y+8)<<line_to(_x+_sx-30,_y+_sy-8)<<move_to(_x+_sx-25,_y+_sy/2 - 5)<<line_to(_x+_sx-20,_y+_sy/2 + 5)<<line_to(_x+_sx-15,_y+_sy/2 - 5)<<line_to(_x+_sx-25,_y+_sy/2 - 5); ///gomb
    textdraw(); ///szöveg kiiratása
}

bool legor::butsel(int mx, int my) const{  /// kivalasztott
    bool res = false;
    if (mx> _x+_sx-30 && mx< _x+_sx-8 && my > _y+8 &&my < _y+_sy-8 ) res = true;
return res;
}

void legor::down(){  ///legordulo grafikai panel
    gout<<color(0,0,0)<<move_to(_x+8,_y+_sy-8)<<box(_sx-16,hany)<<color(255,255,255)<<move_to(_x+10,_y+_sy-6)<<box(_sx-20,hany-4); ///fekete keret, fehér háttér
    gout<<color(0,0,0)<<move_to(_x+_sx-25,_y+_sy+8)<<line_to(_x+_sx-20, _y+_sy)<<line_to(_x+_sx-15,_y+_sy+8)<<line_to(_x+_sx-25,_y+_sy+8)<<move_to(_x+_sx-30,_y+_sy+13)<<line_to(_x+_sx-10,_y+_sy+13); ///felső gomb
    gout<<color(0,0,0)<<move_to(_x+_sx-30,_y+_sy-8+hany-20)<<line_to(_x+_sx-10,_y+_sy-8+hany-20)<<move_to(_x+_sx-30,_y+_sy-8)<<line_to(_x+_sx-30,_y+_sy-10+hany)<<move_to(_x+_sx-25, _y+_sy-8+hany-16)<<line_to(_x+_sx-20,_y+_sy-8+hany-8)<<line_to(_x+_sx-15,_y+_sy-8+hany-16)<<line_to(_x+_sx-25, _y+_sy-8+hany-16); /// alsó gomb és oldalsó sáv
    int hol = 5; /// lehetoségek kiiarasat vezerlo szam |  also koordinata (_x+_sx-30,_y+_sy+70) - (_x+_sx-10 , _y+_sy+90) /// (_x+_sx-30, _y+_sy -8) - (_x+_sx-10,   _y+_sy+13)
    for (int i = mettol; i<lehet.size();i++){
        gout<<color(0,0,0)<<move_to(_x+10,_y+_sy+hol)<<text(lehet[i]);  ///kiiras
        hol+=16; ///lejjebb tolas
        if (_y+_sy+hol > _y+_sy-8+hany) break; /// ha eleri az also hatart break
    }
}

bool legor::ubut(int mx, int my) const{  /// legordulo menu felfele gomb eldontes
    bool res = false;
    if (mx > _x+_sx-30 && mx < _x+_sx-10 && my >  _y+_sy -8 && my < _y+_sy+13 ) res = true;
return res;
}

bool legor::dbut(int mx,int my) const{  /// legordulo menu lefele gomb eldontes
    bool res = false;
    if (mx > _x+_sx-30 && mx < _x+_sx-10 && my >_y+_sy-8+hany-20 && my < _y+_sy-8+hany && lehet.size()>3) res = true;
return res;
}

void legor::wordselected(int mx, int my){  /// melyik szot valasztjuk ki
    int hol = 5;   /// honnantol kezdje nezni
    for (int i = mettol; i<lehet.size();i++){
        if (mx>_x+8 && mx<_x+_sx-30 && my >_y+_sy+hol-8 && _y+_sy+hol+8 > my ) { /// ellenorzes
            melyiket = i;       /// ha siker ezt valasztja
            success = true;     /// visszacsatolás
            break;              /// siker esetén kilepes
        }
        hol+=16;  /// ha nem talalja akkor lejjebb megy
    }
    if (success==true){  /// ha sikeres a valasztas, akkor legordulo menu eltunik
        butselIs++;
        clrscr();
        draw();
    }
}

bool legor::canlist() const{  /// lehet-e gorgetni
    bool res = false;
    if (lehet.size()>meddig) res = true;
return res;
}

bool legor::listsel(int mx,int my) const{  /// rajta van-e a listan
    bool res = false;
    if ((mx>_x+8 && mx<_x+_sx-30 &&my>_y+_sy-8&&my<_y+_sy+90) && canlist()) res= true;
return res;
}

void legor::gorget(event ev){
    if (ev.button==btn_wheelup){        ///felfele gorgetés
        mettol--;
        if (mettol < 1) mettol=1;       ///nem lehet ettol tul gorgetni
        down();
    }
    if (ev.button==btn_wheeldown){      ///lefele gorgetes
        mettol++;
        if (mettol > lehet.size()-meddig) mettol = lehet.size() -meddig;  ///nem lehet tol gorgetni
        down();
    }
}

bool legor::everythingSelected(int mx, int my) const{   ///minden ki van-e valasztva
    bool res = false;
    if (selected(mx,my) || ubut(mx,my) || dbut(mx,my) || listsel(mx, my)) res = true;
return res;
}

void legor::handler(event ev) {
    if (ev.button==btn_left){       ///bal egergomb
        if (butsel(ev.pos_x,ev.pos_y)){  ///legordulo menu, os fuggvennyel
            down();
            butselIs++;
        }
        if (butselIs%2 == 0){   /// ha a legordulo menu gombjara megint kattintunk akkor visszahuzza
            clrscr();
            draw();
        }
        if (ubut(ev.pos_x,ev.pos_y)) { /// legordulo menu felfele gorgetes gomb kattintásra
            mettol--;
            if (mettol < 1) mettol=1;  /// tulgorgetes gatlo
            if (lehet.size()<meddig) mettol=1; /// ez is
            down();
        }
        if (dbut(ev.pos_x,ev.pos_y)) {  /// legordulo menu lefele gombjara kattintas
            mettol++;
            if (mettol > lehet.size()-meddig) mettol = lehet.size() -meddig; ///tulgorgetes gatlo
            if (lehet.size()<meddig) mettol=1; ///ez is
            down();

        }
        if (butselIs%2!=0){             /// ha van legordulo menu
            wordselected(ev.pos_x,ev.pos_y);  /// sikerult-e a valasztas
            success=false;              /// visszacsatolas visszaallitasa
        }
    }
    draw();
    if (butselIs%2!=0) down();
    if (everythingSelected(ev.pos_x,ev.pos_y)) gorget(ev);  ///gorgetes ha ki van valasztva
}
