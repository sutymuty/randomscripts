#ifndef IROSWIDGET_HPP_INCLUDED
#define IROSWIDGET_HPP_INCLUDED

#include "graphics.hpp"
#include "oswidget.hpp"

using namespace std;

///iros widget hpp

class iros : public oswidget{
private:
    string szoveg;      ///a szoveg
    bool can;           ///lehet-e irni
    int mx,my;          ///focus
public:
    iros(double aa, double bb, double cc, double dd,string s);      ///kezdoX kezdoY hosszX hosszY szoveg amivel kezd
    virtual void draw() const;  ///rajzolo
    virtual void handler(genv::event ev); ///kezelo
    virtual void del(); ///torles
    virtual void ad(char a);    ///iras
    string getString() const;   ///sting getter
};

#endif // IROSWIDGET_HPP_INCLUDED
