#include "graphics.hpp"
#include "oswidget.hpp"
#include "gomb.hpp"

using namespace std;
using namespace genv;

///gomb cpp

gomb::gomb(double aa, double bb, double cc, double dd,string szov) : oswidget(aa,bb,cc,dd){
    felirat = szov; ///felirat
    pressed = false; ///akkor true, ha megnyomtak
}

void gomb::draw() const{    ///kirajzol�s
    if (!pressed) gout<<move_to(_x,_y)<<color(0,64,0)<<box(_sx,_sy)<<move_to(_x+_sx/2 - 22,_y+_sy/2 +5)<<color(255,255,255)<<text(felirat);
    if (pressed) gout<<move_to(_x,_y)<<color(64,64,64)<<box(_sx,_sy)<<move_to(_x+_sx/2 - 22,_y+_sy/2 +5)<<color(255,255,255)<<text(felirat);
}

void gomb::handler(event ev){   ///kezelo
    if (selected(ev.pos_x,ev.pos_y) && ev.button==btn_left && !pressed) pressed = !pressed;
    draw();
}

bool gomb::getStatus() const{ ///statusz getter
    return pressed;
}

void gomb::change(){        ///visszaallitashoz
    pressed=!pressed;
}

void gomb::setPressed(){
    pressed=true;
}

void gomb::setNotPressed(){
    pressed=false;
}
